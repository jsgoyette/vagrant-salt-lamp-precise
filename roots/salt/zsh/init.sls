curl:
  pkg:
    - installed

git:
  pkg:
    - installed

zsh:
  pkg:
    - installed

install oh_my_zsh for vagrant:
  cmd.script:
    - source: salt://zsh/install_oh_my_zsh.sh
    - cwd: /home/vagrant
    - user: vagrant
    - cwd: /home/vagrant/
    - creates: /home/vagrant/.oh-my-zsh
    - require:
      - pkg: git
      - pkg: zsh

update theme for vagrant:
  cmd.run:
    - name: "sed -i 's/robbyrussell/pygmalion/' .zshrc"
    - cwd: /home/vagrant
    - unless: "grep pygmalion .zshrc"

turn off auto update for vagrant:
  cmd.run:
    - name: "sed -i 's/# DISABLE_AUTO_UPDATE/DISABLE_AUTO_UPDATE/' .zshrc"
    - cwd: /home/vagrant
    - unless: "grep '^DISABLE_AUTO_UPDATE' .zshrc"

make zsh default:
  cmd.run:
    - name: "sudo chsh $USER -s $(which zsh)"
    - user: vagrant
    - cwd: /home/vagrant/
    - require:
      - pkg: zsh
